/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app
  .controller('AutoCtrl', function ($scope, $modal) {

    $scope.openAutoModal = function () {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: 'views/modals/auto_detail.html',
        controller: 'AutoModalCtrl',
        size: 'lg',
        resolve: {}
      });

      modalInstance.result.then(function (acc) {
      }, function () {
      });

    };

    $scope.openSmsModal = function (account) {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: 'views/modals/sms_modal.html',
        controller: 'SmsModalCtrl',
        size: 'lg',
        resolve: {
          account: function () {
            return account;
          },
          type: function () {
            return 'ACCOUNT';
          }
        }
      });

      modalInstance.result.then(function (acc) {
      }, function () {
      });
    };
  })

  .controller('AutoModalCtrl', function ($scope, $modalInstance) {
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  })
;
