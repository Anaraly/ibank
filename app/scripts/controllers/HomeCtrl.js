/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app
    .controller('HomeCtrl', function ($scope, RequestService, Session, $state, Payment) {

        $scope.favorites = Session.getObject('favs');
        RequestService.post('payment.list_favorites').then(function (result) {
            $scope.favorites = result.favorites;
            Session.setObject('favs', result.favorites);
            RequestService.post('transfer.last').then(function (result) {
                $scope.payments = result.payments;
            });
        });

        $scope.getTransfers = function (val) {
            return RequestService.post('transfer.search', {search: val}).then(function (result) {
                return result.datas;
            });
        };

        $scope.goTo = function (id) {
            Payment.repeat(id);
        };
    });

