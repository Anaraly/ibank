/**
 * Created by asilbek on 11.01.15.
 */
'use strict';

app
    .controller('SigninCtrl', function ($scope, deviceDetector, $state, $translate, $translatePartialLoader, RequestService, Session, AppToast, toaster, AppStatus, AppPopup) {

        Session.clear();

        $scope.lang = Session.get('lang') || 'ru';

        $scope.deviceInfo = this;
        $scope.deviceInfo.data = deviceDetector;
        $scope.deviceInfo.allData = JSON.stringify($scope.deviceInfo.data, null, 2);


         var clientInfo = {
            browser: $scope.deviceInfo.data.browser,
            browser_v: $scope.deviceInfo.data.browser_version,
            os: $scope.deviceInfo.data.os,
            device: $scope.deviceInfo.data.device
        }

        $scope.isLogin = true;
        $scope.outDated = false;
        if(
            (clientInfo.browser == 'firefox' && clientInfo.browser_v <= '27') ||
            (clientInfo.browser == 'chrome' && clientInfo.browser_v <= '27') ||
            (clientInfo.browser == 'opera' && clientInfo.browser_v <= '12') ||
            (clientInfo.browser == 'ie' && clientInfo.browser_v <= '9') ||
            (clientInfo.browser == 'safari' && clientInfo.browser_v <= '4')
        ){
            $scope.outDated = true;
            $scope.isLogin = false;
        }

        $scope.changeLanguage = function (langKey) {
            $translate.use(langKey);
        };

        $scope.isNew = false;
        $scope.isOtp = false;
        $scope.otp = '';
        $translatePartialLoader.addPart('signin');
        $scope.credentials = {'username': '', 'password': ''};

        $scope.login = function () {
            RequestService.post('auth', $scope.credentials).then(function (data) {
                Session.set('sessionId', data.session);
                Session.set('fio', data.name);
                $scope.isNew = data.is_new == 1;
                $scope.isLogin = false;
                $scope.isOtp = data.is_otp == 1;
                if ($scope.isNew) {
                    $scope.isOtp = !$scope.isNew;
                }
                if ($scope.isOtp) {
                    AppToast.pop('info', 'На ваш телефон отправлен одноразовай пароль сессии');
                }
                else {
                    if ($scope.isNew == false) {
                        $scope.otp = '00000';
                        $scope.verify_otp();
                    }
                }
            });
        };

        $scope.psw = {old: '', new1: '', new2: ''};

        $scope.changePsw = function () {
            if (!$scope.psw.old) {
                return;
            }

            if ($scope.psw.new1 != $scope.psw.new2) {
                AppToast.pop('warning', 'passwords are not same');
                return;
            }

            if (!$scope.psw.new1 || !$scope.psw.new2) {
                AppToast.pop('warning', 'fill all fields');
                return;
            }

            var data = {
                old_password: $scope.psw.old, new_password: $scope.psw.new1,
                device: 'web',
                token: Session.get('sessionId')
            };
            RequestService.post('psw_change', data).then(function () {
                $scope.isNew = false;
                $scope.isOtp = true;
            });
        };

        $scope.verify_otp = function () {
            RequestService.post('verify_otp', {otp: $scope.otp}).then(function () {
                $state.go('app.dashboard');
                AppStatus.request();
            }, function (e) {
                if (e.result != -20) {
                    $scope.isOtp = false;
                    $scope.isLogin = true;
                    $scope.credentials = {};
                }
            });
        };

        $scope.$watch('lang', function () {
            Session.set('lang', $scope.lang);
            $scope.changeLanguage($scope.lang);

        });

        $scope.changeLanguage = function (langKey) {
            $translate.use(langKey);
        };
        $scope.ask = function () {
            AppPopup.show('Подтверждение', 'Запросить одноразоввый пароль сессии еще раз?')
                .then(function () {
                    var data = {
                        device: 'web',
                        token: Session.get('sessionId')
                    };
                    RequestService.post('ask_otp', {}).then(function () {
                        AppToast.pop('info', 'На ваш телефон отправлен одноразовай пароль сессии');
                    });
                }, function () {
                });
        };


    })
;
