/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app
    .controller('MessagesCtrl', function ($scope, RequestService, $modal) {

        function getTopics() {
            return RequestService.post('chat.topics');
        };

        getTopics().then(function (result) {
            $scope.topics = result.topics;
        });

        $scope.openTopicModal = function () {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'topic_modal.html',
                controller: 'TopicModalCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (topicName) {
                RequestService.post('chat.start_topic', {name: topicName}).then(function () {
                    getTopics().then(function (result) {
                        $scope.topics = result.topics;
                        $scope.openTopic($scope.topics[0]);
                    });
                });
            }, function () {
                /*do something*/
            });
        };

        $scope.delTopic = function (topic) {
            RequestService.post('chat.end_topic', {id: topic.id}).then(function () {
                //if (topic.id === $scope.topic.id) {
                //    delete  $scope.topic;
                //}
                getTopics().then(function (result) {
                    $scope.topics = result.topics;
                    //delete $scope.topic;
                });
            });
        };

        function getMessages(topicId) {
            RequestService.post('chat.messages', {id: topicId}).then(function (result) {
                $scope.messages = result.messages;
                delete $scope.message;
            });
        }

        $scope.openTopic = function (topic) {
            $scope.topic = topic;
            getMessages(topic.id);
        };

        $scope.addMessage = function () {
            RequestService.post('chat.message', {topic_id: $scope.topic.id, message: $scope.message}).then(function () {
                getMessages($scope.topic.id);
            });
        }
    })
    .controller('TopicModalCtrl', function ($scope, $modalInstance) {
        $scope.topic = {};
        $scope.ok = function () {
            $modalInstance.close($scope.topic.name);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
    .controller('InfoCtrl', function ($scope, RequestService, $modal) {

        $scope.getInfos = function () {
            RequestService.post('info.get_list').then(function (result) {
                $scope.infos = result.data;
            });
        };

        $scope.getInfos();

        $scope.openInfoModal = function (info) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'info_modal.html',
                controller: 'InfoModalCtrl',
                size: 'lg',
                resolve: {
                    id: function () {
                        return info.id;
                    }
                }
            });
        };

        $scope.delInfo = function (info) {
            RequestService.post('info.delete', {id: info.id}).then(function () {
                $scope.getInfos();
            });
        };
    })
    .controller('InfoModalCtrl', function ($scope, $modalInstance, id, RequestService) {
        RequestService.post('info.get', {id: id}).then(function (result) {
            $scope.info = result.data;
        });

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
;

