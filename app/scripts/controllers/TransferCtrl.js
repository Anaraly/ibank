/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app
    .controller('TransferCtrl', function ($scope, $state) {
        $scope.goUrl = function (url) {
            $state.go('app.transfer' + url);
        };

        $scope.isActive = function (url) {
            return $state.current.name.indexOf(url) > -1;
        };
    })
    .controller('TransferSmartCtrl', function ($scope, RequestService, AppToast, $interval, Session) {

        $scope.step = 1;
        $scope.typeCode = 'INTERNAL';
        $scope.doc_date = moment().format('YYYY-MM-DD');

        RequestService.post('transfer.smart_types').then(function (result) {
            $scope.types = result.datas;
        });

        $scope.$watch("typeCode", function () {
            if (!$scope.typeCode) {
                return;
            }

            var data = {
                status: 'OPEN'
            };
            if ($scope.typeCode !== 'INTERNAL') {
                data.currency = 'KGS';
                $scope.isCollapsedKNP = false;
            }
            else {
              $scope.isCollapsedKNP = true;
            }

            RequestService.post('account.shortlist', data).then(function (result) {
                $scope.accounts = result.accounts;
                var p = Session.getObject('transfer');
                if (p) {
                    $scope.typeCode = p.smart_type;
                    $scope.accountId = p.account_id.toString();
                    $scope.amount = p.amount;
                    $scope.doc_id = p.id;
                    $scope.doc_date = moment(p.doc_date).format('YYYY-MM-DD');
                    $scope.rec_name = p.rec_name;
                    $scope.doc_number = p.doc_num;
                    $scope.rec_account = p.rec_account;
                    $scope.rec_inn = p.rec_inn;
                    $scope.rec_okpo = p.rec_okpo;
                    $scope.rec_cf = p.rec_cf;
                    $scope.purpose = p.purpose;
                    $scope.description = p.note;

                    RequestService.post('transfer.ppc_list', {search: p.knp}).then(function (result) {
                        $scope.ppc = result.ppc.length > 0 ? result.ppc[0] : undefined;
                    });
                    if (p.rec_bank) {
                        RequestService.post('transfer.bank_list', {search: p.rec_bank}).then(function (result) {
                            $scope.bank = result.banks.length > 0 ? result.banks[0] : undefined;
                        });
                    }

                    Session.remove('transfer');
                }
            });
        });

        $scope.$watch("accountId", function () {
            delete $scope.ppc;
            if (!$scope.accountId) {
                return;
            }
            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.account = entry;
                }
            });
        });

        $scope.$watch("otp", function () {
            $scope.isConfirmDisabled = !($scope.otp);
        });

        $scope.getBanks = function (val) {
            return RequestService.post('transfer.bank_list', {search: val}).then(function (result) {
                return result.banks;
            });
        };

        $scope.getPpc = function (val) {
            return RequestService.post('transfer.ppc_list', {
                search: val,
                account_id: $scope.account.id
            }).then(function (result) {
                return result.ppc;
            });
        };

        $scope.getKnp = function (val) {
            return RequestService.post('transfer.knp_list', {parent_id: val}).then(function (result) {
                return result.knps;
            });
        };

        RequestService.post('transfer.knp_list').then(function (result) {
            $scope.knps1 = result.knps;
        });

        $scope.setStep = function (step) {
            $scope.step = step;
        };

        $scope.next = function () {

            var data = {
                id: $scope.doc_id,
                account: $scope.account.id,
                doc_date: moment($scope.doc_date).format('YYYY-MM-DD'),
                doc_number: $scope.doc_number,
                rec_name: $scope.rec_name,
                rec_bank: $scope.typeCode === 'INTERNAL' ? undefined : $scope.bank.id,
                rec_account: $scope.rec_account,
                rec_inn: $scope.rec_inn,
                rec_okpo: $scope.rec_okpo,
                rec_cf: $scope.rec_cf,
                knp:  $scope.ppc ? $scope.ppc.id : null,
                type_code: $scope.typeCode,
                amount: $scope.amount,
                purpose: $scope.purpose,
                description: $scope.description
            };

            RequestService.post('transfer.smart', data).then(function (result) {
                $scope.doc_id = result.id;
                $scope.doc_number = result.doc_number;
                $scope.commission = result.commission;
                RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP2'}).then(function (result) {
                    $scope.datas = result.data;
                    $scope.otpHide = result.status != 'OTP';
                    $scope.isConfirmDisabled = !$scope.otpHide;
                    $scope.setStep(2);
                });
            });
        };

        $scope.isDisabled = false;
        $scope.getOtp = function () {
            $scope.isDisabled = true;
            RequestService.post('transfer.send_otp', {id: $scope.doc_id}).then(function () {
                AppToast.pop('success', 'Смс отправлен на ваш телефон.');
                $scope.isDisabled = false;
            }, function (err) {
                $scope.isDisabled = false;
            });
        };


        $scope.checkPayment = function () {
            RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP3'}).then(function (result) {
                $scope.datas = result.data;
                //$scope.setStep(2);
            });
            //RequestService.post('transfer.check', {id: $scope.doc_id}).then(function (result) {
            //    if (result.code) {
            //        $scope.operation_code = result.code;
            //        $scope.secret_word = result.secret_word;
            //        AppToast.pop('success', 'Операция успешно завершена');
            //    }
            //});
        };

        $scope.doConfirm = function () {
            RequestService.post('transfer.confirm', {id: $scope.doc_id, otp: $scope.otp}).then(function () {
                $scope.setStep(3);
                $scope.checkPayment();
            });
        };
    })
    .controller('TransferSendCtrl', function ($scope, RequestService, AppToast, $interval, Session) {

        $scope.step = 1;

        var data = {
            status: 'OPEN'
        };

        RequestService.post('account.shortlist', data).then(function (result) {
            $scope.accounts = result.accounts;
            var p = Session.getObject('transfer');
                if (p) {
                    $scope.accountId = p.account_id.toString();
                    $scope.amount = p.amount;
                    $scope.doc_id = p.id;
                    $scope.rec_name = p.rec_name;
                    $scope.rec_account = p.rec_account;
                    $scope.description = p.note;

                    Session.remove('transfer');
                }
            });


        $scope.$watch("accountId", function () {
            if (!$scope.accountId) {
                return;
            }
            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.account = entry;
                }
            });
        });

        $scope.setStep = function (step) {
            $scope.step = step;
        };

        $scope.next = function () {

            var data = {
                id: $scope.doc_id,
                account: $scope.account.id,
                rec_name: $scope.rec_name,
                rec_account: $scope.rec_account,
                amount: $scope.amount,
                description: $scope.description
            };

            RequestService.post('transfer.send', data).then(function (result) {
                $scope.doc_id = result.id;
                RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP2'}).then(function (result) {
                    $scope.datas = result.data;
                    $scope.otpHide = result.status != 'OTP';
                    $scope.isConfirmDisabled = (!($scope.otpHide));
                    $scope.setStep(2);
                });
            });
        };

        $scope.$watch("otp", function () {
            $scope.isConfirmDisabled = !($scope.otp);
        });

        $scope.isDisabled = false;
        $scope.getOtp = function () {
            $scope.isDisabled = true;
            RequestService.post('transfer.send_otp', {id: $scope.doc_id}).then(function () {
                AppToast.pop('success', 'Смс отправлен на ваш телефон.');
                $scope.isDisabled = false;
            }, function (err) {
                $scope.isDisabled = false;
            });
        };


        $scope.checkPayment = function () {
            RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP3'}).then(function (result) {
                $scope.datas = result.data;
            });
        };

        $scope.doConfirm = function () {
            RequestService.post('transfer.confirm', {id: $scope.doc_id, otp: $scope.otp}).then(function () {
                $scope.setStep(3);
                $scope.checkPayment();
            });
        };
    })
    .controller('TransferMyselfCtrl', function ($scope, RequestService, $interval, AppToast, Session) {

        $scope.step = 1;

        RequestService.post('account.shortlist').then(function (result) {
            $scope.accounts = result.accounts;
            $scope.fromAccounts = result.accounts;
            $scope.toAccounts = result.accounts;

            var p = Session.getObject('transfer');
            if (!p) {
                return;
            }
            $scope.doc_id = p.id;
            $scope.fromDb = true;
            $scope.fromId = p.account_id.toString();
            $scope.toId = p.account_credit_id.toString();
            $scope.amount = p.amount;
            $scope.description = p.note;

            Session.remove('transfer');
        });

        $scope.$watch("fromId", function () {
            $scope.toAccounts = [];
            delete $scope.fromAccount;
            if (!$scope.fromDb) {
                delete $scope.toAccount;
                delete $scope.toId;
            }
            if (!$scope.fromId) {
                return;
            }

            $scope.accounts.forEach(function (entry) {
                if (entry.id == $scope.fromId) {
                    $scope.fromAccount = entry;
                }
            });
            if (!$scope.fromDb) {
                $scope.accounts.forEach(function (entry) {
                    if (entry.id != $scope.fromAccount.id && entry.code == $scope.fromAccount.code) {
                        $scope.toAccounts.push(entry);
                    }
                });
            } else {
                $scope.toAccounts = $scope.accounts;
            }
        });

        $scope.$watch("toId", function () {
            $scope.toAccount = undefined;
            if (!$scope.toId) {
                return;
            }

            $scope.accounts.forEach(function (entry) {
                if (entry.id == $scope.toId) {
                    $scope.toAccount = entry;
                }
            });
        });

        $scope.setStep = function (step) {
            $scope.step = step;
        };

        $scope.next = function () {

            var data = {
                id: $scope.doc_id,
                debit: $scope.fromAccount.id,
                credit: $scope.toAccount.id,
                amount: $scope.amount,
                description: $scope.description
            };

            RequestService.post('transfer.intra', data).then(function (result) {
                $scope.doc_id = result.id;
                $scope.commission = result.commission;
                RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP2'}).then(function (result) {
                    $scope.datas = result.data;
                    $scope.setStep(2);
                });
            });
        };

        $scope.checkPayment = function () {
            RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP3'}).then(function (result) {
                $scope.datas = result.data;
            });
            //RequestService.post('transfer.check', {id: $scope.doc_id}).then(function (result) {
            //    if (result.code) {
            //        $scope.operation_code = result.code;
            //        $scope.secret_word = result.secret_word;
            //        AppToast.pop('success', 'Операция успешно завершена');
            //    }
            //});
        };

       // $scope.doConfirm = function () {
       //     $scope.setStep(3);
       //     $scope.checkPayment();
       // };
        $scope.doConfirm = function () {
            RequestService.post('transfer.confirm', {id: $scope.doc_id}).then(function (result) {
                $scope.setStep(3);
                $scope.checkPayment();
            });
        };
    })
    .controller('TransferTransitCtrl', function ($scope, RequestService, $interval, AppToast, Session) {

        $scope.step = 1;
        RequestService.post('account.shortlist').then(function (result) {
            $scope.accounts = result.accounts;

            var p = Session.getObject('transfer');
            if (!p) {
                return;
            }
            $scope.doc_id = p.id;
            $scope.accountId = p.account_id.toString();
            $scope.RecLastName = p.rec_lastname;
            $scope.RecFirstName = p.rec_firstname;
            $scope.RecPatronymic = p.rec_patronymic;
            $scope.amount = p.amount;
            $scope.description = p.note;

            Session.remove('transfer');
        });

        $scope.$watch("accountId", function () {

            delete $scope.selectedAccount;
            if (!$scope.accountId) {
                return;
            }

            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.selectedAccount = entry;
                }
            });

        });

        $scope.$watch("otp", function () {
            $scope.isConfirmDisabled = !($scope.otp);
        });

        $scope.setStep = function (step) {
            $scope.step = step;
        };

        $scope.next = function () {

            var data = {
                id: $scope.doc_id,
                account: $scope.selectedAccount.id,
                rec_lastname: $scope.RecLastName,
                rec_firstname: $scope.RecFirstName,
                rec_patronymic: $scope.RecPatronymic,
                amount: $scope.amount,
                description: $scope.description
            };

            RequestService.post('transfer.transit', data).then(function (result) {
                $scope.doc_id = result.id;
                $scope.commission = result.commission;
                RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP2'}).then(function (result) {
                    $scope.datas = result.data;
                    $scope.otpHide = result.status != 'OTP';
                    $scope.isConfirmDisabled = !$scope.otpHide;
                    $scope.setStep(2);
                });
            });
        };

        $scope.isDisabled = false;
        $scope.getOtp = function () {
            $scope.isDisabled = true;
            RequestService.post('transfer.send_otp', {id: $scope.doc_id}).then(function (result) {
                AppToast.pop('success', 'Смс отправлен на ваш телефон.');
                $scope.isDisabled = false;
            }, function (err) {
                $scope.isDisabled = false;
            });
        };

        $scope.checkPayment = function () {
            RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP3'}).then(function (result) {
                $scope.datas = result.data;
            });
            //RequestService.post('transfer.check', {id: $scope.doc_id}).then(function (result) {
            //    if (result.code) {
            //        $scope.operation_code = result.code;
            //        $scope.secret_word = result.secret_word;
            //        AppToast.pop('success', 'Операция успешно завершена');
            //    }
            //});
        };

        $scope.doConfirm = function () {
            RequestService.post('transfer.confirm', {id: $scope.doc_id, otp: $scope.otp}).then(function (result) {
                $scope.setStep(3);
                $scope.checkPayment();
            });
        };
    })
    .controller('TransferCardCtrl', function ($scope, RequestService, $interval, AppToast, Session) {

        var accountId = Session.get('accountId') || '';
        $scope.cardId = accountId.toString();
        Session.remove('accountId');

        $scope.step = 1;
        $scope.doc_date = moment().format('YYYY-MM-DD');

        var data = {
                status: 'OPEN',
                currency : 'KGS'
            };

        RequestService.post('account.shortlist', data).then(function (result) {
            $scope.accounts = result.accounts;

            var p = Session.getObject('transfer');
            if (!p) {
                return;
            }
            $scope.doc_id = p.id;
            $scope.accountId = p.account_id.toString();
            $scope.rec_card = p.rec_card;
            $scope.valid_date = p.valid_date;
            $scope.amount = p.amount;
            $scope.description = p.note;

            Session.remove('transfer');
        });

        RequestService.post('transfer.transit_ppc_list').then(function (result) {
            $scope.tppc = result.tppc;
        });

        $scope.$watch("accountId", function () {
            if (!$scope.accountId) {
                return;
            }
            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.account = entry;
                }
            });
        });

        $scope.$watch("otp", function () {
            $scope.isConfirmDisabled = !($scope.otp);
        });

        $scope.setStep = function (step) {
            $scope.step = step;
        };

        $scope.next = function () {

            var data = {
                id: $scope.doc_id,
                account_id: $scope.accountId.toString(),
                rec_card: $scope.rec_card,
                valid_date: $scope.valid_date,
                amount: $scope.amount,
                description: $scope.description
            };

            RequestService.post('transfer.card2card', data).then(function (result) {
                $scope.doc_id = result.id;
                $scope.commission = result.commission;
                RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP2'}).then(function (result) {
                    $scope.datas = result.data;
                    $scope.otpHide = result.status != 'OTP';
                    $scope.isConfirmDisabled = !$scope.otpHide;
                    $scope.setStep(2);
                });
            });
        };
        $scope.isDisabled = false;
        $scope.getOtp = function () {
            $scope.isDisabled = true;
            RequestService.post('transfer.send_otp', {id: $scope.doc_id}).then(function (result) {
                AppToast.pop('success', 'Смс отправлен на ваш телефон.');
                $scope.isDisabled = false;
            }, function (err) {
                $scope.isDisabled = false;
            });
        };

        $scope.checkPayment = function () {
            RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP3'}).then(function (result) {
                $scope.datas = result.data;
            });
            //RequestService.post('transfer.check', {id: $scope.doc_id}).then(function (result) {
            //    if (result.code) {
            //        $scope.operation_code = result.code;
            //        $scope.secret_word = result.secret_word;
            //        AppToast.pop('success', 'Операция успешно завершена');
            //    }
            //});
        };

        $scope.doConfirm = function () {
            RequestService.post('transfer.confirm', {id: $scope.doc_id, otp: $scope.otp}).then(function (result) {
                $scope.setStep(3);
                $scope.checkPayment();
            });
        };
    })
    .controller('DetailCtrl', function ($scope, $state, RequestService, $stateParams, Session, AppPopup, Payment) {

        var id = $stateParams.id;
        if (!id) {
            return;
        }

        function getDetail() {
            RequestService.post('transfer.get_detail', {id: parseInt(id)}).then(function (result) {
                $scope.datas = result.data;
                $scope.status = result.status;
            });
        }

        getDetail();

        $scope.continue = function () {
            RequestService.post('transfer.get', {id: parseInt(id)}).then(function (result) {
                Session.setObject('transfer', result);
                if (result.type == 'SMART') {
                    $state.go('app.transfer.smart');
                }
                if (result.type == 'SEND') {
                    $state.go('app.transfer.send');
                }
                if (result.type == 'SERVICE') {
                    $state.go('app.payment', {serviceId: result.service_id});
                }
                if (result.type == 'MYSELF') {
                    $state.go('app.transfer.myself');
                }
                if (result.type == 'TRANSIT') {
                    $state.go('app.transfer.transit');
                }
                if (result.type == 'EXCHANGE') {
                    $state.go('app.currency.exchange');
                }
                if (result.type == 'CARD') {
                    $state.go('app.transfer.tocard');
                }
            });
        };

        $scope.paymentRepeat = function () {
            Payment.repeat(id);
        };
        $scope.cancel = function () {
            AppPopup.show('Подтверждение', 'Вы уверены в отмене операции?')
                .then(function () {
                    RequestService.post('transfer.cancel', {id: parseInt(id)}).then(function () {
                        getDetail();
                    });
                }, function () {
                });
        };
        $scope.openPdf = function (open) {

            var def = {
                content: []
                , defaultStyle: {
                    font: 'Courier'
                }
                , pageMargins: [10, 10, 10, 10]
            };

            RequestService.post('transfer.pdf', {id: id}).then(function (result) {
                $scope.json = result.datas;

                for (var i in $scope.json) {
                    var s = $scope.json[i].name;
                    def.content.push(s);
                }
                if (open)
                    pdfMake.createPdf(def).open();
                else
                    pdfMake.createPdf(def).download();
            });

            //var docDefinition = {
            //    content: [
            //        {
            //            columns: [
            //                {
            //                    text: 'Төлөө тапшырмасы № 1258 Платежное поручение',
            //                    style: 'header'
            //                },
            //                {
            //                    text: 'Дата: 21.05.2015',
            //                    style: 'header'
            //                },
            //                {
            //                    text: 'Жөнөтүү ыкмасы / Способ отправления _________',
            //                    style: 'header'
            //                },
            //                {
            //                    text: 'Форма коду / Код формы',
            //                    style: 'header'
            //                }
            //            ]
            //        },
            //
            //        {text: 'Дата расчета: 20.06.2016', style: 'subheader'},
            //
            //        {
            //            style: 'table',
            //            table: {
            //                widths: [40, 80, 80, 120, 30, 120],
            //                body: [
            //                    ['ОКПО\n\n', {rowSpan: 4, text: ''}, {
            //                        text: 'Төөлөчү \n Платильщик',
            //                        colSpan: 2,
            //                        rowSpan: 3
            //                    }, '', 'Дебет\n Счет №', ''],
            //                    ['ИНН\n\n', '', {text: '', colSpan: 2}, '', 'Дебет\n Счет №', ''],
            //                    ['Регистр № СФКР\n', '', {text: '', colSpan: 2}, '', 'Дебет\n Счет №', ''],
            //                    ['БИК\n\n', '', {
            //                        text: 'Төөлөчүнүн банкы \n Банк платильщика',
            //                        colSpan: 2
            //                    }, '', 'Дебет\n Счет №', '']
            //                ]
            //            }
            //        }
            //
            //    ],
            //    styles: {
            //        header: {
            //            fontSize: 11,
            //            bold: false,
            //            margin: [0, 0, 20, 10]
            //        },
            //        subheader: {
            //            fontSize: 11,
            //            bold: true,
            //            margin: [0, 10, 0, 5]
            //        },
            //        tableExample: {
            //            margin: [0, 5, 0, 15]
            //        },
            //        table: {
            //            fontSize: 9
            //        }
            //    },
            //    defaultStyle: {
            //        // alignment: 'justify'
            //    }
            //};


        };
    })
;
