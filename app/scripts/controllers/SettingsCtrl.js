/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app.controller('PasswordCtrl', function ($scope, RequestService, AppToast) {

    $scope.psw = {old: '', new1: '', new2: ''};

    $scope.changePsw = function () {
        if (!$scope.psw.old) {
            return;
        }

        if ($scope.psw.new1 != $scope.psw.new2) {
            AppToast.pop('warning', 'passwords are not same');
            return;
        }

        if (!$scope.psw.new1 || !$scope.psw.new2) {
            AppToast.pop('warning', 'fill all fields');
            return;
        }

        var data = {
            old_password: $scope.psw.old,
            new_password: $scope.psw.new1
        };
        RequestService.post('users.change_password', data).then(function () {
            AppToast.pop('success', 'success');
            $scope.psw = {old: '', new1: '', new2: ''};
        });
    };

})
    .controller('InfoMessageCtrl', function ($scope, $modalInstance, title, message) {
        $scope.title = title;
        $scope.message = message;
        $scope.ok = function () {
            $modalInstance.close($scope.account);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
;
