/**
 * Created by Jaynakus on 11.01.15.
 */
'use strict';
app
    .controller('AppCtrl', function ($scope, $rootScope, $translate, $window, $translatePartialLoader, Session, RequestService, $state, StatusClass) {

        $translatePartialLoader.addPart('app');
        $scope.isSmartDevice = function isSmartDevice($window) {
            // Adapted from http://www.detectmobilebrowsers.com
            var ua = $window.navigator.userAgent || $window.navigator.vendor || $window.opera;
            // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
            return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        };

        // add 'ie' classes to html
        var isIE = !!navigator.userAgent.match(/MSIE/i);
        if (isIE) {
            angular.element($window.document.body).addClass('ie');
        }
        if ($scope.isSmartDevice($window)) {
            angular.element($window.document.body).addClass('smart');
        }

        // config
        $scope.app = {
            name: 'Интернет Банк',
            merchant: 'БТА',
            version: '1.0.0',
            // for chart colors
            color: {
                primary: '#7266ba',
                info: '#23b7e5',
                success: '#27c24c',
                warning: '#fad733',
                danger: '#f05050',
                light: '#e8eff0',
                dark: '#3a3f51',
                black: '#1c2b36'
            },
            settings: {
                themeID: 1,
                navbarHeaderColor: 'bg-white-only',
                navbarCollapseColor: 'bg-white-only',
                asideColor: 'bg-white',
                headerFixed: true,
                asideFixed: true,
                asideFolded: false,
                asideDock: false,
                container: false
            }
        };

        $scope.fio = Session.get('fio');

        $scope.isActive = function (url) {
            return $state.current.name.indexOf(url) > -1;
        };

        $scope.getBadges = function () {

            RequestService.post('info.get_info').then(function (result) {
                if (!result.message) {
                    delete $scope.messages;
                }
                else {
                    $scope.messages = result.message;
                }
                if (!result.information) {
                    delete result.information;
                }
                else {
                    $scope.infos = result.information;
                }
            });
        };

        $scope.goRoute = function (route) {
            $state.go(route);
            $scope.getBadges();
        };

        $scope.getBadges();

        $rootScope.statusClass = StatusClass;

        // angular translate
        $scope.lang = {isopen: false};
        $scope.langs = {'kg': 'Кыргызча', 'ru': 'Русский', 'en': 'English'};
        $scope.selectLang = $scope.langs[Session.get('lang')];  //$scope.langs[$translate.use()];

        $scope.setLang = function (langKey) {
            // set the current lang
            $scope.selectLang = $scope.langs[langKey];
            // You can change the language during runtime
            $translate.use(langKey);
            $scope.lang.isopen = !$scope.lang.isopen;

            Session.set('lang', langKey);

            RequestService.post('users.change_language', {}).then(function (result) {

            });
        };
    })
    .directive('sendOnePass', function () {
        return { template: '{{ "ONE_PASS" | translate }}' }
    })
    .directive('askOpt', function () {
        return { template: '{{ "ONE_PASS" | translate }}' }
    })
;
