/**
 * Created by asilbek on 22.01.15.
 */
app.controller('PaymentCtrl', function ($scope, RequestService, $stateParams, AppToast, $interval, Session) {

        var serviceId = $stateParams.serviceId;
        if (!serviceId) {
            return;
        }

        RequestService.post('payment.get_service', {id: serviceId}).then(function (result) {
            $scope.service = result.service;
            RequestService.post('account.shortlist').then(function (result) {
                $scope.accounts = result.accounts;
                var payment = Session.getObject('transfer') || null;

                if (payment) {
                    $scope.doc_id = payment.id;
                    $scope.requisite = payment.rec_account;
                    $scope.amount = payment.amount;
                    $scope.description = payment.note;
                    $scope.accountId = payment.account_id.toString();
                    Session.remove('transfer');
                }
            });
        });

        $scope.step = 1;
        $scope.doc_date = moment().format('YYYY-MM-DD');

        $scope.$watch("accountId", function () {

            delete $scope.selectedAccount;
            if (!$scope.accountId) {
                return;
            }

            if (!$scope.accounts) return;

            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.selectedAccount = entry;
                }
            });

        });

        $scope.$watch("otp", function () {
            $scope.isConfirmDisabled = !($scope.otp);
        });

        $scope.setStep = function (step) {
            $scope.step = step;
        };

        $scope.next = function () {

            var data = {
                id: $scope.doc_id,
                account: $scope.selectedAccount.id,
                service: $scope.service.id,
                requisite: $scope.requisite,
                amount: $scope.amount,
                description: $scope.description
            };

            RequestService.post('payment.pay', data).then(function (result) {
                $scope.doc_id = result.id;
                $scope.commission = result.commission;
                RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP2'}).then(function (result) {
                    $scope.datas = result.data;
                    $scope.otpHide = (!(result.status === 'OTP'));
                    $scope.isConfirmDisabled = (!($scope.otpHide));
                    $scope.setStep(2);
                });
            });
        };

        $scope.isDisabled = false;
        $scope.getOtp = function () {
            $scope.isDisabled = true;
            RequestService.post('transfer.send_otp', {id: $scope.doc_id}).then(function () {
                AppToast.pop('success', 'Смс отправлен на ваш телефон.');
                $scope.isDisabled = false;
            }, function (err) {
                $scope.isDisabled = false;
            });
        };

        $scope.checkPayment = function () {
            RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP3'}).then(function (result) {
                $scope.datas = result.data;
            });

            //RequestService.post('transfer.check', {id: $scope.doc_id}).then(function (result) {
            //    if (result.code) {
            //        $scope.operation_code = result.code;
            //        $scope.secret_word = result.secret_word;
            //        AppToast.pop('success', 'Операция успешно завершена');
            //    }
            //});
        };
        $scope.doConfirm = function () {
            RequestService.post('transfer.confirm', {id: $scope.doc_id, otp: $scope.otp}).then(function () {
                $scope.setStep(3);
                $scope.checkPayment();
            });
        };
    }
)
;
