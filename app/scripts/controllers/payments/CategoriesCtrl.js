/**
 * Created by asilbek on 22.01.15.
 */
app.controller('CategoriesCtrl', function ($scope, RequestService, Session, $state) {

        $scope.categories = Session.getObject('cats');

        RequestService.post('payment.category_list').then(function (result) {
            $scope.categories = result.categories;
            Session.setObject('cats', result.categories);
        });

        RequestService.post('payment.last').then(function (result) {
            $scope.payments = result.payments;
        });

        $scope.goPayment = function (payment) {
            Session.setObject('payment', payment);
            $state.go('app.payment', {serviceId: payment.service_id});
        };
    }
)
    .controller('ServicesCtrl', function ($scope, RequestService, $stateParams, AppToast) {

        var categoryId = $stateParams.categoryId;
        if (!categoryId) {
            return;
        }

        $scope.services = [];
        RequestService.post('payment.service_list', {category_id: categoryId}).then(function (result) {
            $scope.services = result.services;
        });

        $scope.makeFavorite = function (f) {
            var url = f.favorite ? 'payment.del_favorite' : 'payment.add_favorite';
            RequestService.post(url, {service: f.id})
                .then(function () {
                    AppToast.pop('success', 'Операция успешно завершена');
                    f.favorite = !f.favorite;
                }, function (err) {
                });
        };
    }
)
;
