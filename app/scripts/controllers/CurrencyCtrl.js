/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app
    .controller('CurrencyCtrl', function ($scope, $state) {
        $scope.goUrl = function (url) {
            $state.go('app.currency' + url);
        };

        $scope.isActive = function (url) {
            return $state.current.name.indexOf(url) > -1;
        };
    })
    .controller('CurrencyRateCtrl', function ($scope, RequestService) {
        RequestService.post('currency.list').then(function (result) {
            $scope.currencies = result.currencies;
        });
    })
    .controller('CurrencyExchangeCtrl', function ($scope, RequestService, AppToast, $interval, $modal, AppPopup, $state, Session) {

        var data = Session.getObject('data');
        if (data) {
            $scope.step = data.step;
            RequestService.post('transfer.get_detail', {id: data.id, code: 'STEP2'}).then(function (result) {
                $scope.datas = result.data;
            });

            Session.remove('data');
        }
        else {
            $scope.step = 1;
        }


        $scope.sell_amount = 0;
        $scope.buy_amount = 0;

        RequestService.post('account.shortlist').then(function (result) {
            $scope.accounts = result.accounts;
            $scope.fromAccounts = result.accounts;
            $scope.toAccounts = result.accounts;

            var p = Session.getObject('transfer');
            if (!p) {
                return;
            }

            $scope.fromDb = true;
            $scope.doc_id = p.id;
            $scope.fromId = p.account_id.toString();
            $scope.toId = p.account_buy.toString();
            $scope.sell_amount = p.amount_sell;
            $scope.buy_amount = p.amount_buy;
            $scope.description = p.note;

            Session.remove('transfer');

        });

        $scope.$watch("fromId", function () {
            $scope.toAccounts = [];
            delete $scope.fromAccount;

            if (!$scope.fromDb) {
                delete $scope.toAccount;
                delete $scope.toId;
            }
            if (!$scope.fromId) {
                return;
            }

            $scope.accounts.forEach(function (entry) {
                if (entry.id == $scope.fromId) {
                    $scope.fromAccount = entry;
                }
            });
            if (!$scope.fromDb) {
                $scope.accounts.forEach(function (entry) {
                    if (entry.id != $scope.fromAccount.id && entry.code != $scope.fromAccount.code) {
                        $scope.toAccounts.push(entry);
                    }
                });
            } else {
                $scope.toAccounts = $scope.accounts;
            }
        });

        $scope.$watch("toId", function () {
            $scope.toAccount = undefined;
            if (!$scope.toId) {
                return;
            }

            $scope.accounts.forEach(function (entry) {
                if (entry.id == $scope.toId) {
                    $scope.toAccount = entry;
                }
            });
        });

        $scope.$watch("sell_amount", function () {
            if (!$scope.sell_amount) {
                return;
            }
            $scope.buy_amount = 0;
        });

        $scope.$watch("buy_amount", function () {
            if (!$scope.buy_amount) {
                return;
            }
            $scope.sell_amount = 0;
        });

        $scope.$watch("otp", function () {
            $scope.isConfirmDisabled = !($scope.otp);
        });

        $scope.setStep = function (step) {
            $scope.step = step;
        };

        $scope.calculate = function () {

            var data = {
                id: $scope.doc_id,
                sell_id: $scope.fromAccount.id,
                buy_id: $scope.toAccount.id,
                sell_amount: $scope.sell_amount,
                buy_amount: $scope.buy_amount,
                description: $scope.description
            };

            RequestService.post('currency.get_rate', data).then(function (result) {
                $scope.doc_id = result.id;
                $scope.bam = result.buy_amount;
                $scope.sam = result.sell_amount;
                $scope.rate = result.rate;
                if (result.status == 'WAITING') {
                    AppPopup.show('Информация', 'Ваша операция находится на подтверждение казначейство')
                        .then(function () {
                            $state.go('app.currency.operation');
                        }, function () {
                            $state.go('app.currency.operation');
                        });
                }
                RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP2'}).then(function (result) {
                    $scope.datas = result.data;
                    $scope.setStep(2);
                });
            });
        };

        $scope.isDisabled = false;
        $scope.getOtp = function () {
            $scope.isDisabled = true;
            RequestService.post('transfer.send_otp', {id: $scope.doc_id}).then(function (result) {
                AppToast.pop('success', 'Смс отправлен на ваш телефон.');
                $scope.isDisabled = false;
            }, function (err) {
                $scope.isDisabled = false;
            });
        };

        $scope.checkPayment = function () {
            RequestService.post('transfer.get_detail', {id: $scope.doc_id, code: 'STEP3'}).then(function (result) {
                $scope.datas = result.data;
                $scope.otpHide = (!(result.status === 'OTP'));
                $scope.isConfirmDisabled = (!($scope.otpHide));
            });
            //RequestService.post('transfer.check', {id: $scope.doc_id}).then(function (result) {
            //    if (result.code) {
            //        $scope.operation_code = result.code;
            //        $scope.secret_word = result.secret_word;
            //        AppToast.pop('success', 'Операция успешно завершена');
            //    }
            //});
        };

        $scope.doConfirm = function () {
            RequestService.post('transfer.confirm', {id: $scope.doc_id, otp: $scope.otp}).then(function (result) {
                $scope.setStep(3);
                $scope.checkPayment();
            });
        };
    })
    .controller('CurrencyOperationsCtrl', function ($scope, RequestService, Session, $state) {
        RequestService.post('currency.operations').then(function (result) {
            $scope.operations = result.operations;
        });

        $scope.goExchange = function (id) {
            if (!id) return;
            Session.setObject('data', {id: id, step: 2});
            $state.go('app.currency.exchange');
        };
    })
;