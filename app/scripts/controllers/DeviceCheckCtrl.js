'use strict';

app.controller('deviceCheck',['$scope', 'deviceDetector',function($scope, deviceDetector){
    $scope.deviceInfo = this;
    $scope.deviceInfo.data = deviceDetector;
    $scope.deviceInfo.allData = JSON.stringify($scope.deviceInfo.data, null, 2);


     var clientInfo = {
        browser: $scope.deviceInfo.data.browser,
        browser_v: $scope.deviceInfo.data.browser_version,
        os: $scope.deviceInfo.data.os,
        device: $scope.deviceInfo.data.device
    }
}])