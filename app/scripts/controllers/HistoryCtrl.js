/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app
    .controller('HistoryCtrl', function ($scope, $state, RequestService, AppPopup) {

        $scope.datbeg = moment().subtract(1, 'months').format('YYYY-MM-DD');
        $scope.datend = moment().format('YYYY-MM-DD');

        RequestService.post('account.shortlist').then(function (result) {
            $scope.accounts = result.accounts;
            $scope.accounts.splice(0, 0, {id: '', name: '- Все'});
        });

        RequestService.post('currency.short_list').then(function (result) {
            $scope.currencies = result.currencies;
            $scope.currencies.splice(0, 0, {id: '', code: '- Все'});
        });

        RequestService.post('history.type_list').then(function (result) {
            $scope.types = result.data;
            $scope.types.splice(0, 0, {id: '', name: '- Все -'});
        });

        //$scope.accountId = '';
        //$scope.currencyId = '';

        $scope.$watch("accountId", function () {
            if (!$scope.accountId) {
                $scope.disableCurrency = false;
                return;
            }
            $scope.disableCurrency = true;
        });

        $scope.$watch("currencyId", function () {
            if (!$scope.currencyId) {
                $scope.disableAccount = false;
                return;
            }
            $scope.disableAccount = true;
        });

        $scope.getHistory = function () {
            var data = {
                account: $scope.accountId || undefined,
                currency: $scope.currencyId || undefined,
                datbeg: moment($scope.datbeg).format('YYYY-MM-DD'),
                datend: moment($scope.datend).format('YYYY-MM-DD'),
                operation_type: $scope.typeId || undefined,
                filter: $scope.filtr || undefined
            };
            RequestService.post('history.get_list', data).then(function (result) {
                $scope.data = result.data;
            });
        };

        $scope.cancel = function (id) {
            AppPopup.show('Подтверждение', 'Вы уверены в отмене операции?')
                .then(function () {
                    RequestService.post('transfer.cancel', {id: id}).then(function () {
                        $scope.getHistory();
                    });
                }, function () {
                });
        }
    })
;
