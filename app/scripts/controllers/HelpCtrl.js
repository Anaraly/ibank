/**
 * Created by asilbek on 22.01.15.
 */
'use strict';
app
    .controller('HelpCtrl', function ($scope, $state) {
        $scope.url = $state.current.name;

        $scope.goUrl = function (url) {
            $scope.url = url;
            $state.go('app.help' + url);
        };

        $scope.isActive = function (url) {
            return $scope.url.indexOf(url) > -1;
        };
    })
;
