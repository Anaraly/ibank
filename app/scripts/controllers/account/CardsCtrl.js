/**
 * Created by asilbek on 22.01.15.
 */
app
    .controller('CardsCtrl', function ($scope, RequestService, AppToast, $modal, Session, $state) {

        function getCards() {
            RequestService.post('card.listing').then(function (result) {
                $scope.accounts = result.accounts;
            });
        }

        getCards();

        $scope.renameCard = function (card) {
            RequestService.post('card.rename', {card: card.id, name: $scope.acc.name}).then(function () {
                $scope.acc.renaming = false;
                card.name = $scope.acc.name;
                AppToast.pop('success', 'Операция успешно завершена');
            });
        };

        $scope.openModal = function (account) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'views/modals/rename_modal.html',
                controller: 'AccountRenameCtrl',
                size: 'lg',
                resolve: {
                    account: function () {
                        return account;
                    },
                    path: function () {
                        return 'card.rename';
                    }
                }
            });

            modalInstance.result.then(function (acc) {
                account.name = acc.name;
            }, function () {
                /*do something*/
            });
        };

        $scope.openSmsModal = function (account) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'views/modals/sms_modal.html',
                controller: 'SmsModalCtrl',
                size: 'lg',
                resolve: {
                    account: function () {
                        return account;
                    },
                    type: function () {
                        return 'CARD';
                    }

                }
            });

            modalInstance.result.then(function (acc) {
                getCards();
            }, function () {
                getCards();
            });
        };

        $scope.openLimitModal = function (account) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'CardLimitModal.html',
                controller: 'CardLimitCtrl',
                size: 'md',
                resolve: {
                    account: function () {
                        return account;
                    }
                }
            });

            modalInstance.result.then(function (acc) {
                account.name = acc.name;
            }, function () {
                /*do something*/
            });
        };

        $scope.openBlockModal = function (account) {

            if (account.status != 'OPEN') {
                return;
            }

            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'CardBlockModal.html',
                controller: 'CardBlockCtrl',
                size: 'md',
                resolve: {
                    account: function () {
                        return account;
                    }
                }
            });

            modalInstance.result.then(function () {
                getCards();
            }, function () {
                getCards();
            });
        };

        $scope.goRoute = function (routeName, card) {
            Session.set('accountId', card.id);
            $state.go(routeName);
        }

    })
    .controller('SmsModalCtrl', function ($scope, $modalInstance, account, type, RequestService, AppToast) {
        $scope.account = account;

        RequestService.post('sms.channels').then(function (result) {
            $scope.channels = result.channels;
        });

        $scope.$watch("channelId", function () {
            $scope.fees = [];
            if (!$scope.channelId) {
                return;
            }
            RequestService.post('sms.fees', {channel_id: parseInt($scope.channelId)}).then(function (result) {
                $scope.fees = result.fees;
            });
        });

        function getList() {
            RequestService.post('sms.notifications', {type: type, id: $scope.account.id}).then(function (result) {
                $scope.notifications = result.notifications;
                delete $scope.channelId;
                delete $scope.address;
                delete $scope.feeId;
            });
        }

        getList();

        $scope.addNotification = function () {
            var data = {
                type: type,
                id: $scope.account.id,
                channel: parseInt($scope.channelId),
                address: $scope.address,
                fee: parseInt($scope.feeId)
            };

            RequestService.post('sms.add', data).then(function () {
                getList();
            });
        };

        $scope.delNotification = function (entry) {
            RequestService.post('sms.delete', {id: entry.id}).then(function () {
                getList();
            });
        };

        $scope.ok = function () {
            RequestService.post(path, {id: account.id, name: $scope.acc.name}).then(function () {
                $scope.account.name = $scope.acc.name;
                AppToast.pop('success', 'операция успешно завершена');
            });
            $modalInstance.close($scope.account);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
    .controller('CardBlockCtrl', function ($scope, $modalInstance, account, RequestService, AppToast) {
        $scope.account = account;

        RequestService.post('card.block_reasons').then(function (result) {
            $scope.reasons = result.data;
        });

        $scope.block = function () {
            var data = {
                id: $scope.account.id,
                reason_id: parseInt($scope.reasonId),
                password: $scope.password
            };

            RequestService.post('card.block', data).then(function () {
                $modalInstance.close();
            });
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
    .controller('CardLimitCtrl', function ($scope, $modalInstance, account, RequestService) {
        $scope.card = account;

        RequestService.post('limit.period_list').then(function (result) {
            $scope.periods = result.periods;
        });

        function getList() {
            RequestService.post('limit.get_list', {type: 'CARD', id: $scope.card.id}).then(function (result) {
                $scope.limits = result.limits;
                delete $scope.periodId;
                delete $scope.amount;
            });
        }

        getList();

        $scope.addLimit = function () {
            var data = {
                type: 'CARD',
                id: $scope.card.id,
                amount: parseFloat($scope.amount),
                period: parseInt($scope.periodId)
            };

            RequestService.post('limit.add', data).then(function () {
                getList();
            });
        };

        $scope.delLimit = function (entry) {
            RequestService.post('limit.delete', {id: entry.id}).then(function () {
                getList();
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
    .controller('CardStatement', function ($scope, RequestService, Session) {
        var accountId = Session.get('accountId') || '';
        accountId = accountId.toString();
        Session.remove('accountId');

        $scope.datbeg = moment().subtract(1, 'months').format('YYYY-MM-DD');
        $scope.datend = moment().format('YYYY-MM-DD');

        $scope.account = {};

        RequestService.post('card.list_cards').then(function (result) {
            $scope.accounts = result.cards;

            if (accountId) {
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.accountId = accountId;
                    });
                }, 500);
            }
        });

        $scope.$watch("accountId", function () {
            if (!$scope.accountId) {
                return;
            }
            $scope.transactions = [];
            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.account = entry;
                }
            });
        });

        $scope.getTransactions = function () {
            $scope.transactions = [];
            if (!$scope.account.id) {
                return;
            }

            var data = {
                datbeg: moment($scope.datbeg).format('YYYY-MM-DD'),
                datend: moment($scope.datend).format('YYYY-MM-DD'),
                account_id: $scope.account.id
            };
            RequestService.post('card.history', data).then(function (result) {
                $scope.incoming = result.incoming;
                $scope.outgoing = result.outgoing;
                $scope.transactions = result.operations;
            });
        };
    })
    .controller('CardMini', function ($scope, RequestService, Session) {
        var accountId = Session.get('accountId') || '';
        accountId = accountId.toString();
        Session.remove('accountId');

        $scope.account = {};

        RequestService.post('card.list_cards').then(function (result) {
            $scope.accounts = result.cards;

            if (accountId) {
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.accountId = accountId;
                    });
                }, 500);
            }
        });

        $scope.$watch("accountId", function () {
            if (!$scope.accountId) {
                return;
            }
            $scope.transactions = [];
            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.account = entry;
                }
            });
        });

        $scope.getTransactions = function () {
            $scope.transactions = [];
            if (!$scope.account.id) {
                return;
            }

            var data = {
                card_id: $scope.account.id
            };
            RequestService.post('card.mini_statment', data).then(function (result) {
                $scope.transactions = result.card_statement;
            });
        };
    })
;
