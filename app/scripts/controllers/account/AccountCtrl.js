/**
 * Created by asilbek on 22.01.15.
 */
app
    .controller('AccountCtrl', function ($scope, RequestService, $state, Session, $modal) {
        function getAccounts() {
            RequestService.post('account.listing').then(function (data) {
                $scope.accounts = data.accounts;
            });
        }

        getAccounts();

        $scope.goRoute = function (account) {
            if (account) {
                Session.set('accountId', account.id);
            }
            $state.go('app.statement');
        };

        $scope.openModal = function (account) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'views/modals/rename_modal.html',
                controller: 'AccountRenameCtrl',
                size: 'md',
                resolve: {
                    account: function () {
                        return account;
                    },
                    path: function () {
                        return 'account.rename';
                    }
                }
            });

            modalInstance.result.then(function (acc) {
                account.name = acc.name;
            }, function () {
            });
        };

        $scope.openSmsModal = function (account) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'views/modals/sms_modal.html',
                controller: 'SmsModalCtrl',
                size: 'lg',
                resolve: {
                    account: function () {
                        return account;
                    },
                    type: function () {
                        return 'ACCOUNT';
                    }
                }
            });

            modalInstance.result.then(function (acc) {
                getAccounts();
            }, function () {
                getAccounts();
            });
        };

        $scope.openInfoModal = function (account) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'account_info.html',
                controller: 'AccountInfoCtrl',
                size: 'md',
                resolve: {
                    account: function () {
                        return account;
                    }
                }
            });

            modalInstance.result.then(function (acc) {
                account.name = acc.name;
            }, function () {
                /*do something*/
            });
        };
    })
    .controller('AccountRenameCtrl', function ($scope, $modalInstance, account, path, toaster, RequestService, AppToast) {
        $scope.account = account;
        $scope.acc = {};
        $scope.ok = function () {
            RequestService.post(path, {id: account.id, name: $scope.acc.name}).then(function (data) {
                $scope.account.name = $scope.acc.name;
                toaster.pop({
                    type: 'success',
                    body: 'operation-success',
                    bodyOutputType: 'directive',
                    timeout: 3000
               });
            });
            $modalInstance.close($scope.account);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
    .directive('operationSuccess', function () {
        return { template: '{{ "OPERATION_SUCCESS" | translate }}' }
    })
    .controller('StatementCtrl', function ($scope, RequestService, Session, AppToast) {

        var accountId = Session.get('accountId') || '';
        accountId = accountId.toString();
        Session.remove('accountId');

        $scope.datbeg = moment().subtract(1, 'months').format('YYYY-MM-DD');
        $scope.datend = moment().format('YYYY-MM-DD');

        $scope.account = {};

        RequestService.post('account.shortlist').then(function (result) {
            $scope.accounts = result.accounts;

            if (accountId) {
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.accountId = accountId;
                    });
                }, 500);
            }
        });

        $scope.$watch("accountId", function () {
            if (!$scope.accountId) {
                return;
            }
            $scope.transactions = [];
            $scope.accounts.forEach(function (entry) {
                if (entry.id == parseInt($scope.accountId)) {
                    $scope.account = entry;
                }
            });
        });

        $scope.transactions = [];
        $scope.getTransactions = function () {
            $scope.transactions = [];
            if (!$scope.account.id) {
                AppToast.pop('warning', 'Выберите счет');
                return;
            }

            var data = {
                datbeg: moment($scope.datbeg).format('YYYY-MM-DD'),
                datend: moment($scope.datend).format('YYYY-MM-DD'),
                account_id: $scope.account.id
            };
            RequestService.post('account.history', data).then(function (result) {
                $scope.incoming = result.incoming;
                $scope.outgoing = result.outgoing;
                $scope.transactions = result.operations;
            });
        };
    })

    .controller('AccountInfoCtrl', function ($scope, $modalInstance, account, RequestService) {
        RequestService.post('account.info', {id: account.id}).then(function (result) {
            $scope.data = result.data;
        });

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    })
;
