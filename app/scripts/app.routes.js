'use strict';

app
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider
            .otherwise('/signin');
        $stateProvider
            .state('app', {
                abstract: true,
                url: '/app',
                templateUrl: 'views/app.html',
                controller: 'AppCtrl'
            })
            .state('signin', {
                url: '/signin',
                templateUrl: 'views/sign.html',
                controller: 'SigninCtrl'
            })

            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: 'views/dashboard.html',
                controller: 'HomeCtrl'
            })

            .state('app.account', {
                url: '/account',
                templateUrl: 'views/account.html',
                controller: 'AccountCtrl'
            })
            .state('app.statement', {
                url: '/accounts/statement',
                templateUrl: 'views/accounts/statement.html',
                controller: 'StatementCtrl'
            })
            .state('app.transfer', {
                url: '/accounts/transfer',
                templateUrl: 'views/accounts/transfer.html',
                controller: 'TransferCtrl'
            })
            .state('app.transfer.smart', {
              url: '/smart',
              templateUrl: 'views/accounts/smart.html',
              controller: 'TransferSmartCtrl'
            })
            .state('app.transfer.send', {
              url: '/send',
              templateUrl: 'views/accounts/send.html',
              controller: 'TransferSendCtrl'
            })
            .state('app.transfer.tocard', {
                url: '/tocard',
                templateUrl: 'views/accounts/tocard.html',
                controller: 'TransferCardCtrl'
            })
            .state('app.transfer.myself', {
                url: '/myself',
                templateUrl: 'views/accounts/myself.html',
                controller: 'TransferMyselfCtrl'
            })
            .state('app.transfer.international', {
                url: '/international',
                templateUrl: 'views/accounts/international.html'

            })
            .state('app.transfer.transit', {
                url: '/transit',
                templateUrl: 'views/accounts/transit.html',
                controller: 'TransferTransitCtrl'
            })

            .state('app.history', {
                url: '/history',
                templateUrl: 'views/history.html',
                controller: 'HistoryCtrl'
            })
            .state('app.cards', {
                url: '/cards',
                templateUrl: 'views/cards.html',
                controller: 'CardsCtrl'
            })
            .state('app.cardstatement', {
                url: '/cardstatement',
                templateUrl: 'views/cardstatement.html',
                controller: 'CardStatement'
            })
            .state('app.cardmini', {
                url: '/cardmini',
                templateUrl: 'views/card/cardmini.html',
                controller: 'CardMini'
            })
            .state('app.categories', {
                url: '/categories',
                templateUrl: 'views/categories.html',
                controller: 'CategoriesCtrl'
            })
            .state('app.services', {
                url: '/categories/:categoryId',
                templateUrl: 'views/services.html',
                controller: 'ServicesCtrl'
            })
            .state('app.payment', {
                url: '/payment/:serviceId',
                templateUrl: 'views/payment.html',
                controller: 'PaymentCtrl'
            })
            .state('app.settings', {
                url: '/settings',
                templateUrl: 'views/settings.html'
            })
            .state('app.settings.password', {
                url: '/password',
                templateUrl: 'views/settings/password.html',
                controller: 'PasswordCtrl'
            })
            .state('app.mobile', {
                url: '/categories/mobile',
                templateUrl: 'views/services/mobile.html'
            })
            .state('app.deposite', {
                url: '/deposite',
                templateUrl: 'views/deposite.html'
            })
            .state('app.credits', {
                url: '/credits',
                templateUrl: 'views/credits.html'
            })
            .state('app.plan', {
                url: '/credit-plan',
                templateUrl: 'views/credits/plan.html'
            })
            .state('app.currency', {
                url: '/currency',
                templateUrl: 'views/currency/currency.html',
                controller: 'CurrencyCtrl'
            })
            .state('app.currency.rate', {
                url: '/rate',
                templateUrl: 'views/currency/rate.html',
                controller: 'CurrencyRateCtrl'
            })
            .state('app.currency.exchange', {
                url: '/exchange',
                templateUrl: 'views/currency/exchange.html',
                controller: 'CurrencyExchangeCtrl'
            })
            .state('app.currency.operation', {
                url: '/operation',
                templateUrl: 'views/currency/operation.html',
                controller: 'CurrencyOperationsCtrl'
            })
            .state('app.autopayment', {
                url: '/autopayment',
                templateUrl: 'views/autopayment.html',
                controller: 'AutoCtrl'
            })
            .state('app.rate', {
                url: '/rate',
                templateUrl: 'views/rate.html',
                controller: 'RateCtrl'
            })
            .state('app.faq', {
                url: '/faq',
                templateUrl: 'views/faq.html'
            })
            .state('app.user-settings', {
                url: '/settings',
                templateUrl: 'views/user-settings.html'
            })
            .state('app.messages', {
                url: '/messages',
                templateUrl: 'views/messages.html',
                controller: 'MessagesCtrl'
            })
            .state('app.information', {
                url: '/information',
                templateUrl: 'views/information.html',
                controller: 'InfoCtrl'
            })
            .state('app.help', {
                url: '/help',
                templateUrl: 'views/help.html',
                controller: 'HelpCtrl'
            })
            .state('app.oferta', {
                url: '/oferta',
                templateUrl: 'views/oferta.html'
            })
            .state('app.details', {
                url: '/details/:id',
                templateUrl: 'views/details.html',
                controller:'DetailCtrl'
            })

    });
