/**
 * Created by admin on 25.03.2015.
 */
'use strict';

Date.prototype.isValid = function () {
    // An invalid date object returns NaN for getTime() and NaN is the only
    // object not strictly equal to itself.
    return this.getTime() === this.getTime();
};
app.filter('cbsDate', function ($filter) {
    return function (input) {
        if (!input) {
            return '';
        }
        var _date = new Date(input);
        if (!_date.isValid()) {
            _date = moment(input);
            _date = _date.toDate();
        }
        return $filter('date')(_date, 'dd.MM.yyyy');
    };
})
    .filter('cbsDateTime', function ($filter) {
        return function (input) {
            if (!input) {
                return '';
            }

            var _date = new Date(input);
            if (!_date.isValid()) {
                _date = moment(input);
                _date = _date.toDate();
            }
            return $filter('date')(_date, 'dd.MM.yyyy HH:mm');
        };
    })

    .filter('cbsDateLong', function ($filter) {
        return function (input) {
            if (!input) {
                return '';
            }
            var _date = new Date(input);
            if (!_date.isValid()) {
                _date = moment(input);
                _date = _date.toDate();
            }
            return $filter('date')(_date, 'dd MMMM yyyy');
        };
    })

    .filter('cbsNumber', function ($filter) {
        return function (input) {
            return isNaN(input) ? input : $filter('number')(input, 2);
        };
    })
    .filter('cbsRate', function ($filter) {
        return function (input) {
            return isNaN(input) ? input : $filter('number')(input, 4);
        };
    })
    .filter('cbsStatus', function ($filter, AppStatus) {
        return function (input) {
            return AppStatus.get(input);
        };
    })
;
