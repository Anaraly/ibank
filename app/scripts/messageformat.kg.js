'use strict';

MessageFormat.locale.kg = function (n) {
    var r10, r100;
    if ((n % 10) === 1 && (n % 100) !== 11) {
        r10 = n % 10;
        r100 = n % 100;

        if (r10 === 1 && r100 !== 11) {
            return 'one';
        }
    }
    if ((n % 10) >= 2 && (n % 10) <= 4 &&
        ((n % 100) < 12 || (n % 100) > 14) && n === Math.floor(n)) {

        if (r10 >= 2 && r10 <= 4 && (r100 < 12 || r100 > 14) && n === Math.floor(n)) {
            return 'few';
        }
    }
    if ((n % 10) === 0 || ((n % 10) >= 5 && (n % 10) <= 9) ||
        ((n % 100) >= 11 && (n % 100) <= 14) && n === Math.floor(n)) {
        return 'many';
    }
    return 'other';
};