'use strict';
var app;
app = angular.module('cbsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'ui.router',
    'pascalprecht.translate',
    'FBAngular',
    'toaster',
    'angular-loading-bar',
    'ui.bootstrap',
    'base64',
    'angularImgFallback',
    'ui.mask',
    'ng.deviceDetector',
    'ui.utils.masks'
])
    .run(function ($rootScope, $translate, $state) {
        $rootScope.$on('$translatePartialLoaderStructureChanged', function () {
            $translate.refresh();
        });
        //app.api = 'http://localhost:5050/';
         app.api = 'http://dev.bta.kg/';
        $rootScope.$on('$stateChangeStart', function (event, next) {

        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
            $state.previous = fromState;
        });
        pdfMake.fonts = {
            Courier: {
                normal: 'Courier.ttf',
                bold: 'Courier.ttf',
                italics: 'Courier.ttf',
                bolditalics: 'Courier.ttf'
            }
        };
    })
    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: 'i18n/{lang}.json'
        }).addInterpolation('$translateMessageFormatInterpolation')
            .preferredLanguage('ru')
            .fallbackLanguage('ru')
            .useCookieStorage()
            .useSanitizeValueStrategy(null);
    }])
    .value('cgBusyDefaults', {
        message: 'Идет загрузка',
        backdrop: true,
        delay: 300,
        minDuration: 700
    });

    //Изменение языковых флажков, замена классов
    function changelanguage(selectbox) {
        $(selectbox)
            .removeClass('ru')
            .removeClass('en')
            .removeClass('kg')
        $(selectbox).addClass($(selectbox).val());
    }
