/**
 * Created by Jaynakus on 18.01.15.
 */
'use strict';
angular.module('cbsApp')
    .directive('cbsDict', function (BObjectService) {
        return {
            link: function (scope, element) {
                var table = element.attr('cbs-dict');
                BObjectService.list(table).then(function (data) {
                    scope[table] = data;
                });
            }
        };
    });