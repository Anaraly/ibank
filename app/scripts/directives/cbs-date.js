'use strict';
angular.module('cbsApp')
    .directive('cbsDate', function () {
        return {
            restrict: 'AEC',
            scope: {
                ngModel: '=?',
                required: '=?',
                ngDisabled: '=?'
            },
            link: function (scope) {
                scope.isOpen = false;
                if (scope.ngDisabled) {
                    scope.open = null;
                } else {
                    scope.open = function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        scope.isOpen = true;
                    };
                }
            },
            template: '<p class="input-group">' +
            '<input type="text" class="form-control" datepicker-popup="dd.MM.yyyy" placeholder="дд.мм.гггг"' +
            'ng-model="ngModel" is-open="isOpen" datepicker-options="{startingDay:1}"' +
            'close-text="закрыть" current-text="сегодня" clear-text="очистить" ng-disabled="ngDisabled"/>' +
            '<span class="input-group-btn">' +
            '<button type="button" class="btn btn-default" ng-click="open($event)">' +
            '<i class="glyphicon glyphicon-calendar"></i></button>' +
            '</span>' +
            '</p>'
        };
    });