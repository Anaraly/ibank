/**
 * Created by Jaynakus on 19.01.15.
 */
'use strict';
angular.module('cbsApp')
    .directive('cbsClient', function ($compile, ClientService) {
        return {
            restrict: 'AEC',
            scope: {
                modeldisplay: '=?',
                modelret: '=?',
                clientType: '=?',
                modelobject: '=?',
                ngDisabled: '=?'
            },
            link: function (scope) {
                scope.current = 0;
                scope.selected = false;
                scope.da = function (txt) {
                    scope.ajaxClass = 'loadImage';
                    ClientService.find(txt, 5, scope.clientType)
                        .then(function (response) {
                            scope.TypeAheadData = response.data.clients;
                            scope.ajaxClass = '';
                        });
                };

                scope.handleSelection = function (key, val, object) {
                    scope.modelret = key;
                    scope.modeldisplay = val;
                    scope.modelobject = object;
                    scope.current = 0;
                    scope.selected = true;
                };

                scope.isCurrent = function (index) {
                    return scope.current === index;
                };

                scope.setCurrent = function (index) {
                    scope.current = index;
                };
                scope.$watch('modelret', function (value) {
                    if (value) {
                        ClientService.find(value.toString()).
                            then(function (response) {
                                if (response.data.clients.length === 0) {
                                    scope.modeldisplay = null;
                                    scope.modelobject = null;
                                    return;
                                }
                                var client = response.data.clients[0];
                                scope.modeldisplay = client.last_name + ' ' + client.first_name;
                            });
                    } else {
                        scope.modeldisplay = null;
                        scope.modelobject = null;
                    }
                });
            },
            template: '<input type="text" ng-model="modeldisplay" ng-keyup="da(modeldisplay)" class="form-control"  ng-keydown="selected=false" ' +
            'style="width:100%;" ng-class="ajaxClass" ng-disabled="ngDisabled"> ' +
            '<div class="list-group table-condensed overlap" ng-hide="!modeldisplay.length || selected" style="width:100%"> ' +
            '<a href="javascript:;" class="list-group-item noTopBottomPad" ng-repeat="item in TypeAheadData|filter:model  track by $index" ' +
            'ng-click="handleSelection(item.id,item.last_name,item)" style="cursor:pointer" ' +
            'ng-class="{active:isCurrent($index)}" ' +
            'ng-mouseenter="setCurrent($index)"> ' +
            ' {{item.id}} ' +
            '<i>{{item.last_name}} {{item.first_name}}</i> ' +
            '</a> ' +
            '</div> ' +
            '</input> '
        };
    });