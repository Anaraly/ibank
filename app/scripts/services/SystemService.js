/**
 * Created by asilbek on 25.05.15.
 */
'use strict';
app
    .service('Session', function ($window) {
        return {
            set: function (key, value) {
                $window.sessionStorage[key] = value;
            },
            get: function (key) {
                return $window.sessionStorage[key] || '';
            },
            setObject: function (key, value) {
                $window.sessionStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.sessionStorage[key] || null);
            },
            remove: function (key) {
                try {
                    $window.sessionStorage.removeItem(key);
                } catch (err) {
                }
            },
            clear: function () {
                try {
                    $window.sessionStorage.clear();
                } catch (err) {
                }
            }
        };
    })
    .service('AppToast', function (toaster) {
        return {
            pop: function (type, message) {
                toaster.clear();
                toaster.pop(type, '', message);
            }
        };
    })
    .service('Payment', function (RequestService, $state, Session) {
        return {
            repeat: function (paymentId) {
                RequestService.post('transfer.get', {id: parseInt(paymentId)}).then(function (result) {
                    result.id = null;
                    Session.setObject('transfer', result);
                    if (result.type == 'SMART') {
                        $state.go('app.transfer.smart');
                    }
                    if (result.type == 'SERVICE') {
                        $state.go('app.payment', {serviceId: result.service_id});
                    }
                    if (result.type == 'MYSELF') {
                        $state.go('app.transfer.myself');
                    }
                    if (result.type == 'TRANSIT') {
                        $state.go('app.transfer.transit');
                    }
                    if (result.type == 'EXCHANGE') {
                        $state.go('app.currency.exchange');
                    }
                    if (result.type == 'CARD') {
                        $state.go('app.transfer.tocard');
                    }
                });
            }
        };
    })
    .service('AppPopup', function ($modal) {
        return {
            show: function (title, message) {
                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'views/modals/info_modal.html',
                    controller: 'InfoMessageCtrl',
                    size: 'md',
                    resolve: {
                        title: function () {
                            return title;
                        },
                        message: function () {
                            return message;
                        }
                    }
                });

                return modalInstance.result;
            }
        };
    })
    .service('AppStatus', function (RequestService, Session) {
        return {
            request: function () {
                RequestService.post('info.get_statuses').then(function (result) {
                    Session.setObject('statuses', result.data);
                });
            },

            get: function (statusCode) {
                if (!statusCode) {
                    return;
                }

                var statuses = Session.getObject('statuses');
                if (statuses) {
                    for (var id in statuses) {
                        var status = statuses[id];
                        if (status.code === statusCode) {
                            return status.name;
                        }
                    }
                }
                else {
                    RequestService.post('info.get_statuses').then(function (result) {
                        Session.setObject('statuses', result.data);
                    });
                }
                return statusCode;
            }
        };
    })
    .service('BObjectService', function ($rootScope, RequestService, $cacheFactory, $q) {
        var bObject = {};
        var dictionaryCache = $cacheFactory('dictionaries');

        bObject.list = function (crud, filter, nocache) {
            var defer = $q.defer();
            var data = null;
            if (nocache === undefined || nocache === false) {
                data = dictionaryCache.get(crud + JSON.stringify(filter));
            }
            if (data) {
                defer.resolve(data);
            } else {
                RequestService.post('object.full', {crud: crud, filter: filter})
                    .then(function (data) {
                        dictionaryCache.put(crud + JSON.stringify(filter), data.list);
                        defer.resolve(data.list);
                    }, function () {
                        defer.reject();
                    });
            }
            return defer.promise;
        };
        bObject.clear = function () {
            dictionaryCache.removeAll();
        };
        bObject.add = function (crud, item) {
            bObject.clear();
            return RequestService.post('object.add', {crud: crud, bobject: item});
        };
        bObject.remove = function (crud, id) {
            bObject.clear();
            return RequestService.post('object.delete', {crud: crud, id: id});
        };
        return bObject;
    })
    .service('StatusClass', function () {
        return {
            get: function (statusCode) {
                if (statusCode == 'EXECUTED')
                    return 'success';
                if (statusCode == 'WAITING')
                    return 'warning';
                if (statusCode == 'LOCKED')
                    return 'danger';
                if (statusCode == 'REJECTED')
                    return 'danger';
                if (statusCode == 'CANCELLED')
                    return 'info';
                if (statusCode == 'WORK')
                    return 'primary';
                if (statusCode == 'SUSPENDED')
                    return 'warning';
                if (statusCode == 'OPEN')
                    return 'success';
                if (statusCode == 'CLOSED')
                    return 'danger';

                return 'default';
            }
        };
    })
;