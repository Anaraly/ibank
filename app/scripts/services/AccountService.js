/**
 * Created by asilbek on 13.09.2015.
 */
'use strict';
app
    .service('AccountService', function () {
        var srv = this;
        var account = null;
        var accountId = null;

        srv.setAccountId = function (value) {
            accountId = value;
        };

        srv.getAccountId = function () {
            return accountId;
        };

        srv.setAccount = function (value) {
            account = value;
        };

        srv.getAccount = function () {
            return account;
        };
        return srv;
    });