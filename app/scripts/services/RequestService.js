/**
 * Created by asilbek on 25.05.2015.
 */

'use strict';
app
    .service('RequestService', function ($http, $rootScope, $q, Session, AppToast, $state, $base64) {
        return {
            post: function (methodName, data, silent, timeOutSec) {

                timeOutSec = timeOutSec || 60;

                var timeout = $q.defer(),
                    result = $q.defer(),
                    timedOut = false;

                setTimeout(function () {
                    timedOut = true;
                    timeout.resolve();
                }, (100000 * timeOutSec));

                if (silent) {
                }
                data = data || {};
                data.device = 'web';
                data.lang = Session.get('lang') || 'ru';
                var sessionId = Session.get('sessionId');
                sessionId = sessionId || '';
                $http.defaults.headers.common['Authorization'] = 'Basic ' + $base64.encode('web' + ':' + sessionId);

                $rootScope.httPromise = $http({
                    method: 'post',
                    url: app.api + methodName,
                    data: data,
                    cache: false,
                    timeout: timeout.promise
                }).success(function (data, status, headers, config) {
                    if (!silent) {

                    }
                    if (data.result === 0) {
                        result.resolve(data);
                    }
                    else if (data.result == 1000) { // USER_NOT_AUTHORIZED
                        result.reject(data);
                        AppToast.pop('warning', data.message || 'Сессия закрыта');
                        $state.go('signin', {is_otp: 0});
                    }
                    else if (data.result > 0) {
                        AppToast.pop('warning', data.message);
                        result.reject(data);
                    }
                    else if (data.result < 0) {
                        AppToast.pop('error', data.message);
                        result.reject(data);
                    }
                }).error(function (data, status, headers, config) {
                    if (!silent) {
                    }
                    if (timedOut) {
                        if (!silent) {
                        }
                        result.reject({
                            error: 'timeout',
                            message: 'Request took longer than ' + timeOutSec + ' seconds.'
                        });

                        AppToast.pop('warning', 'Request took longer than ' + timeOutSec + ' seconds.');
                    } else {
                        if (!silent) {
                        }
                        AppToast.pop('error', 'Ошибка подключения');
                        $state.go('signin');
                        result.reject(data);
                    }
                });

                return result.promise;
            }
        };
    }).factory('poll', function (RequestService) {
    var defaultPollingTime = 10000;
    var polls = {};

    return {
        startPolling: function (name, url, data, pollingTime, callback) {
            // Check to make sure poller doesn't already exist
            if (!polls[name]) {
                var poller = function () {
                    RequestService.post(url, data, true).then(callback);
                };
                poller();
                polls[name] = setInterval(poller, pollingTime || defaultPollingTime);
            }
        },
        stopPolling: function (name) {
            clearInterval(polls[name]);
            delete polls[name];
        }
    };
});